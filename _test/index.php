<pre>
<?php
require 'phpmailer/PHPMailerAutoload.php';

$mail = new PHPMailer();

$mail->isSMTP();						// Set mailer to use SMTP
$mail->Host = 'smtp.gmail.com';		// Specify main and backup server
$mail->SMTPDebug  = 2;
$mail->SMTPAuth = true;				// Enable SMTP authentication
$mail->Username = 'info@xsoft.org';	// SMTP username
$mail->Password = 'xsoftoffice2013';	// SMTP password
$mail->SMTPSecure = 'ssl';			// Enable encryption, 'ssl' also accepted
$mail->Port = 465;

$mail->SetFrom('info@xsoft.org', 'Xsoft');
$mail->Subject = "Test message";
$mail->MsgHTML('hello world');

$mail->AddAddress('rustam@otono.ru', "Rustam Kuzmani");

if(!$mail->Send()) {
	echo "Mailer Error: " . $mail->ErrorInfo;
} else {
	echo "Message sent!";
}
?>
</pre>