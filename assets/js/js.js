$(document).ready(function () {

	if($('body:eq(0)').hasClass('iphone'))
	{
		$('meta[name="viewport"]').attr('content', 'width='+screen.width+', initial-scale=1.0, maximum-scale=1.0, user-scalable=0');
	}

	$(window).resize(function() {
		$('.middle-block').css({
			'position':'relative',
			'top':($(window).height() - $('.middle-block').outerHeight())/2 + 'px'
		});
	});
	$(window).resize();


	function centerBlock()
	{
		if ($(window).width() < 1000)
		{
			$('.middle-block-disposal').css({
				position:'static',
				padding: '5%'
			});

		} else if($('body:eq(0)').hasClass('mobile') && $(window).width() > 1000) {
			$('.middle-block-disposal').css({
				position:'static',
				padding: '5%'
			});
		} else {
			$('.middle-block-disposal').css({
				position:'relative',
				padding: '5%',
				'top':($(window).height() - $('.middle-block-disposal').outerHeight())/2 + 'px'
			});
		}
	}

	$(window).resize(function () {
		centerBlock();
	});

	centerBlock();

	$(window).resize(function() {
		$('.middle-block-portfolio').css({
			'position':'relative',
			'top':($(window).height() - $('.middle-block-portfolio').outerHeight())/2 + 'px'
		});
	});
	$(window).resize();


	function centerTeam()
	{
		if ($(window).width() < 1000)
		{
			$('.middle-block-team').css({
				position:'static',
				padding: '5%'
			});
			
		} else {
			$('.middle-block-team').css({
				position:'relative',
				padding: '5%',
				'top':($(window).height() - $('.middle-block-team').outerHeight())/2 + 'px'
			});
		}
	}

	$(window).resize(function () {
		centerTeam();
	});

	centerTeam();


	function centerCont()
	{
		if ($(window).width() < 1000)
		{
			$('.middle-block-contacts').css({
				position:'static',
				padding:'5%'
			});

		} else {
			$('.middle-block-contacts').css({
				position:'relative',
				padding: '5%',
				'top':($(window).height() - $('.middle-block-contacts').outerHeight())/2 + 'px'
			});
		}
	}

	$(window).resize(function () {
		centerCont();
	});

	centerCont();


	/* Top Menu 768px */
		$('.topNavMob').click(function(){
			if(!$('#menu').hasClass('active'))
			{
				$('#menu').slideDown('fast', function(){
					$('#menu').addClass('active');
				});
				$('.topNavMob span').addClass('active');
			} else {
				$('#menu').slideUp('fast', function(){
					$('#menu').removeClass('active');
				});
				$('.topNavMob span').removeClass('active');
			}
		});


	/* MODAL WIN */

		var popWin = $('.modalWin');
		var popBg = $('.modalBg');
		var body = $('body');
		var btn = $('.zakaz > div > button');
		var HeightDoc = $(document).height();
		var WidthDoc = $(document).width();

		btn.click(function() {
			var index = $(this).parents('.details-info').find('.zakaz button').index(this);
			var index_site = $('.details-info').index($(this).parents('.details-info'));

			var title_type = $(this).parents('.details-info').find('h2:eq('+index+')').text().toLowerCase();
			var title_site = $('.rate h2:eq('+index_site+')').text().toLowerCase();
			title_site = (title_site == 'корпоративный') ? title_site+' сайт' : title_site;

			if(index == 0) {
				stars_count = 3;
			} else if(index == 1) {
				stars_count = 4;
			} else if(index == 2) {
				stars_count = 5;
			}

			$('#modal-request .site-type').text(title_type);
			$('#modal-request .site-category').text(title_site);
			$('#modal-request .stars span').attr('class','star s-'+stars_count);

			$(this).addClass('active');

			if(popWin.hasClass('iphone'))
			{
				popWin.css('top', $(document).scrollTop()-90).fadeIn(150, function(){});
			} else {
				popWin.fadeIn(150, function(){});
			}
			popBg.css({'width': WidthDoc,'height': HeightDoc});
			popBg.fadeIn(150);
			body.css({'overflow':'hidden'});


			/* Checked services */
			var i = $('.details-info').index($(this).parents('.details-info'));
			var j = $('.details-info:eq('+i+') .zakaz button').index(this);

			var service_list = '';
			$('.details-info:eq('+i+') ul._add:eq('+j+') input:checked').each(function(){
				sep = (service_list !== '') ? ',' : '';
				service_list += sep+$(this).attr('service_id');
			});

			$('#contact-form input[name="services"]').val(service_list);
			$('#contact-form input[name="category"]').val($(this).attr('category'));
			$('#contact-form input[name="type"]').val($(this).attr('type'));

			if(!popWin.hasClass('iphone'))
			{
				$('#modal-request').addClass('flipped');
				$('#modal-request input[type="text"]:eq(0)').focus();
			}
		});

		$(window).resize(function() {
			if(!popWin.hasClass('iphone'))
			{
				popWin.css({
					'position' :'fixed',
					'left':($(window).width() - popWin.outerWidth())/2,
					'top':($(window).height() - popWin.outerHeight())/2,
					'overflow':'auto'
				});
			} else {
				var k = ($(window).width() <= 320) ? 90 : 130;
				popWin.css({
					'position' :'fixed',
					'left':($(window).width() - popWin.outerWidth())/2,
					'top':$(document).scrollTop()-k,
					'overflow':'auto'
				});
			}
			popBg.css({'width':'100%', 'height':'100%'});
		});
		$(window).resize();

		popBg.click(function() {
			$('#modal-request').removeClass('flipped');
			btn.removeClass('active');
			body.css({'overflow':'auto'});
			popWin.fadeOut(150);
			popBg.fadeOut(150);
		});
		$(document).bind('keyup', function(e) {
			if(e.keyCode==27) {
				$('#modal-request').removeClass('flipped');
				btn.removeClass('active');
				body.css({'overflow':'auto'});
				popWin.fadeOut(150);
				popBg.fadeOut(150);
			}
		});

		$('#closer').click(function(){
			body.css({'overflow':'auto'});
			popWin.fadeOut(150);
			popBg.fadeOut(150);
		});

	/* PLACEHOLDER */
	function hasPlaceholderSupport() {
		var input = document.createElement('input');
		return ('placeholder' in input);
	};

	if (!hasPlaceholderSupport()) {
		$('[placeholder]').focus(function() {
			var input = $(this);
			if (input.val() == input.attr('placeholder')) {
				input.val('');
				input.removeClass('placeholder');
			}
		}).blur(function() {
			var input = $(this);
			if (input.val() == '' || input.val() == input.attr('placeholder')) {
				input.addClass('placeholder');
				input.val(input.attr('placeholder'));
			}
		}).blur();
	};

	/* Scroll and pagination */
	if($('body:eq(0)').hasClass('desktop') && $('section').length > 0)
	{
		var body_animated = false;
		var animation_start = false;
		var click_nav = false;
		$(window).scroll();

		$(document).keydown(function(event){
			switch(event.keyCode)
			{
				case 38:
					if(!body_animated && $('#side-nav a.active').parent().prev().find('a').length > 0)
					{
						body_animated = true;
						$('#side-nav a.active').parent().prev().find('a').click();
					}
					return false;
					break;

				case 40:
					if(!body_animated && $('#side-nav a.active').parent().next().find('a').length > 0)
					{
						body_animated = true;
						$('#side-nav a.active').parent().next().find('a').click();
					}
					return false;
					break;
			}
		});

		$('a[data-link]').click(function(){
			click_nav = true;
			var section = $(this).attr('data-link');

			var scroll_to = $('section[data-section="'+section+'"]').offset().top+'px';
			$('nav a[data-link]').removeClass('active');
			$('nav a[data-link="'+section+'"]').addClass('active');

			$('html, body').animate({ scrollTop: scroll_to }, 400, 'swing', function(){
				click_nav = false;
				if(section == 4)
				{
					animateTeam();
				} else {
					$(window).scroll();
				}
				body_animated = false;
			});
			return false;
		});

		$('#preview').css({ 'background-position': 'center ' + ($(document).scrollTop() * 0.7)+'px' });

		if($('body [data-section]').length > 0)
		{
			var last_scroll = $(document).scrollTop();
			$(window).scroll(function(){
				if(!click_nav)
				{
					min_top = $('section[data-section="2"]').offset().top - 1;

					if($(document).scrollTop() <= min_top)
					{
						$('#secondary-nav').fadeOut('fast');
					} else {
						$('#secondary-nav').fadeIn('fast');
					}

					var w_height = $(window).height();
					var current_section;

					$('section').each(function(i){
						if(($(document).scrollTop() + w_height) >= $(this).offset().top)
						{
							wheight = $(window).height();
							vis_top = $(document).scrollTop();
							vis_bottom = $(document).scrollTop() + $(window).height();
							section_top = $(this).offset().top;
							section_bottom = ($(this).offset().top + $(this).height());

							if(vis_top > section_top)
							{
								visible = section_bottom - vis_top;
							} else if(vis_top < section_top) {
								visible = vis_bottom - section_top;
							}

							visible_p = Math.round(visible * 100 / wheight);

							if(visible_p >= 70)
							{
								$('nav a[data-link]').removeClass('active');
								$('nav a[data-link="'+(i+1)+'"]').addClass('active');

								if($('section:eq('+i+')').attr('id') == 'team')
								{
									animateTeam();
								}
							} else if($('section:eq('+i+')').attr('id') == 'team' && visible_p <= 10) {
								//animation_start = false;
								//prepareAnimation();
							} else if($('section:eq('+i+')').attr('id') == 'disposal' && visible_p <= 10 && visible_p > 0) {
								prepare_disposal();
							}
						}
					});

					// Disposal
					$('.anim-disp').each(function(i){
						//console.log(($(document).scrollTop() + $(window).height()) +' >= '+ $(this).offset().top);
						if(($(document).scrollTop() + $(window).height()) >= ($(this).offset().top + $(this).height()))
						{
							if($(document).scrollTop() > ($(this).offset().top + $(this).height()))
							{
								//prepare_disposal(i);
							} else {
								animate_disposal(i);
							}
						} else {
							if(($(document).scrollTop() + $(window).height()) < $(this).offset().top)
							{
								//prepare_disposal(i);
							}
						}
					});

					last_scroll = $(document).scrollTop();
				}

				/* Parallax */
				$('#preview').css({ 'background-position': 'center ' + ($(document).scrollTop() * 0.7)+'px' });
				$('#portfolio').css({ 'background-position': 'center ' + (($(document).scrollTop() - $('#portfolio').offset().top) * 0.5)+'px' });
				//console.log($(document).scrollTop()+'px');
			});
		}

		/* Team */
		function array_shuffle(array) {
			var currentIndex = array.length, temporaryValue, randomIndex;
			while (0 !== currentIndex) {
				randomIndex = Math.floor(Math.random() * currentIndex);
				currentIndex -= 1;
				temporaryValue = array[currentIndex];
				array[currentIndex] = array[randomIndex];
				array[randomIndex] = temporaryValue;
			}
			return array;
		}

		function prepareAnimation()
		{
			$('#team li .title').each(function(){
				$(this).css('height', $(this).height()+'px');
				$(this).find('div').css('top', -$(this).parent().height()/2+'px');
			});
			$('#team .foto img').attr('style','width: 1px; height: 1px;');
			//prepared = true;
		}
		prepareAnimation();

		var photos = new Array();
		i = 0;
		function bounce_team()
		{
			var j = photos[i];

//			$('#team .foto:eq('+j+') img').hide().css({ 'width': 126+'px', 'height': 126+'px', 'margin-top': 0 }).fadeIn(1000, function(){
//				$('#team li:eq('+j+') .title div').css({ 'top': 0 }, 500, 'easeOutBounce');
//			});

			$('#team .foto:eq('+j+') img').animate({ 'width': 126+'px', 'height': 126+'px', 'margin-top': 0 }, 700, 'easeOutBounce', function(){

			});
			setTimeout(function(){
				$('#team li:eq('+j+') .title div').animate({ 'top': 0 }, 500, 'easeOutBounce');
			}, 200);

			i++;
			if(i <= photos.length)
			{
				setTimeout(function(){ bounce_team(); }, 10);
			} else {
				i = 0;
			}
		}

		function animateTeam(){
			if(!animation_start)
			{
				var arr = new Array();

				for(var i = 0; i <= $('#team .foto img').length; i++)
				{
					arr[i] = i;
				}
				arr = array_shuffle(arr);

				i = 0;
				for(var n in arr)
				{
					photos[i] = arr[n];
					i++;
				}

				bounce_team();
			}
			animation_start = true;
		};

		/* Disposal */
		var disposal_animated = new Array();
		var disposal_prepared = new Array();
		var disposal_prepared_total = false;
		function prepare_disposal()
		{
			$('.anim-disp').each(function(i){
				if(!disposal_prepared[i])
				{
					var icon_left = ($(window).width() - $('.middle-block-disposal').width()) / 2 + $(this).find('img').width();
					var text_right = ($(window).width() - $('.middle-block-disposal').width()) / 2 + $(this).find('div').width();
					$(this).find('img').css('left', -icon_left+'px');
					$(this).find('div').css('right', -text_right+'px');
					//disposal_animated[i] = false;
					disposal_prepared[i] = true;
				}
			});
		}
		prepare_disposal();

		var anim_count = 0;
		function animate_disposal(i)
		{
			if(!disposal_animated[i])
			{
				$('.anim-disp:eq('+i+') img').animate({ left: 0 }, 500, 'easeOutQuint');
				$('.anim-disp:eq('+i+') div').animate({ right: 0 }, 500, 'easeOutQuint');

				disposal_animated[i] = true;
				//disposal_prepared[i] = false;
			}
		}
	}

	/* TAB client-info Scroll */
	//console.log('width :'+$(window).width()+'height :'+$(window).height());
	var slider_section_width, current = 0, prev_current = 0, pos = 0, speed = 500, blocks_count = 3, offset_start, offset_new, last_horizontal;

	function prepare_slider()
	{
		var width = ($(window).width() <= 320) ? $(window).width() - 80 : $(window).width() - 220;
		var padding = ($(window).width() <= 320) ? 40 : 110;
		$('#slider-wrap').css('width',$(window).width());
		$('#slideContent').css({'width': $(window).width()*$('#slideContent .slide_container').length, 'padding-left': padding + 'px'});
		$('#slideContent .slide_container').css({ 'width': width });

		slider_section_width = width;
		offset_start = window.pageYOffset;

		var position = -(slider_section_width * current);
		$('#slideContent').css('-webkit-transition-duration', '0s');
		$('#slideContent').css('-webkit-transform', 'translate3d('+position+'px,0px,0px)');

	}

	prepare_slider();

	$(window).resize(function(){ prepare_slider(); });

	if($('#slideContent').length > 0)
	{
		$('#slideContent').swipe({
			triggerOnTouchEnd : true,
			swipeStatus : function(event, phase, direction, distance){
				offset_new = window.pageYOffset;

				//console.log(phase+' = '+direction+' = '+distance);

				if(direction == 'left' || direction == 'right')
				{
					last_horizontal = direction;
				}

				if(phase == 'move' && (direction == 'left' || direction == 'right') && offset_new == offset_start)
				{
					var duration = 0;
					pos = (last_horizontal == 'left') ? (slider_section_width * current) + distance : (slider_section_width * current) - distance;

					slide_blocks(pos, duration);
				}
				else if(phase == 'cancel' && offset_new == offset_start)
				{
					offset_start = pageYOffset;
					//console.log('cancel'+' = '+phase+' = '+direction+' = '+distance+' = '+(slider_section_width * current));
					slide_blocks(slider_section_width * current, speed);
				}
				else if(phase == 'end' && direction)
				{
					prev_current = current;

					if(offset_new == offset_start)
					{
						if(last_horizontal == 'right')
						{
							current = Math.max(current-1, 0);
							slide_blocks(slider_section_width * current, speed);
						}
						else if(last_horizontal == 'left')
						{
							current = Math.min(current+1, blocks_count-1);
							slide_blocks(slider_section_width * current, speed);
						}
					}

					offset_start = pageYOffset;

					if(prev_current !== current)
					{
						if($('ul.rate li.active').length > 0)
						{
							$('ul.rate li.active').removeClass('active');
							$('ul.rate li:eq('+current+')').addClass('active');

							$('.details-info').hide();
							$('.details-info:eq('+current+')').show();
						}
						//$('.details-info').hide();
					}

					$('#pagination span.active').removeClass('active');
					$('#pagination li:eq('+current+') span').addClass('active');
				}
			},
			tap: function(event, phase, direction, distance){
				$('ul.rate li.active').removeClass('active');
				$('ul.rate li:eq('+current+')').addClass('active');

				$('.details-info').hide();
				$('.details-info:eq('+current+')').show();

				$('html, body').animate({ scrollTop: $('ul.rate li:eq('+current+') h2').offset().top-20 }, 500);
			},
			allowPageScroll: 'vertical'
		});

		function slide_blocks(distance, duration, test)
		{
			$('#slideContent').css('-webkit-transition-duration', (duration/1000).toFixed(1) + 's');
			var pos = (distance < 0 ? '' : '-') + Math.abs(distance).toString();
			$('#slideContent').css('-webkit-transform', 'translate3d('+ pos +'px,0px,0px)');
		}
	}

	// Show info
	$('ul.rate li').click(function(){
		var i = $('ul.rate li').index(this);
		var width = $('.bd').width();

		$('.details-info').css('width', width);

		var triangle_positions = new Array();
		if(width >= 900)
		{
			triangle_positions[0] = 140;
			triangle_positions[1] = 440;
			triangle_positions[2] = 740;
		} else if(width < 900) {
			triangle_positions[0] = 100;
			triangle_positions[1] = 320;
			triangle_positions[2] = 540;
		}

		//$('ul.rate li.active').removeClass('active');
		//$('ul.rate li:eq('+i+')').addClass('active');

		if($('ul.rate li.active-list').length > 0 && !$(this).hasClass('active-list'))
		{
			$('ul.rate li.active-list .fotoImg:eq(1)').fadeOut();
			$('ul.rate li.active-list .fotoImg:eq(0)').fadeIn();
			$('ul.rate li.active-list').removeClass('active-list');
		}

		$('ul.rate li:eq('+i+')').addClass('active-list');
		$('ul.rate li:eq('+i+') .fotoImg:eq(0)').fadeOut();
		$('ul.rate li:eq('+i+') .fotoImg:eq(1)').fadeIn();

		if(!$('#details-wrap').hasClass('opened'))
		{
			$('#details-wrap').addClass('opened');
			$('#details-wrap').show();

			$('#triangle').show();

			$('html, body').animate({ scrollTop: $('ul.rate').offset().top-20 }, 500);
			$('#details-wrap').css({ left: -((width+150)*i) });
			$('#triangle').css({ left: triangle_positions[i]+'px' });
			$('#details-wrap').css('height', $('#tab'+(i+1)).height());
		} else {
			$('#details-wrap').animate({ left: -((width+150)*i) }, 700, 'easeOutBack');
			$('#triangle').animate({ left: triangle_positions[i]+'px' }, 700, 'easeOutBack', function(){ $('#details-wrap').css('height', $('#tab'+(i+1)).height()); });
		}
	});

	/* center Footer Iphone */
	function set_iphone_footer()
	{
		var k = ($(window).height() > 320) ? 80 : 40;

		$('#contacts-footer').css({
			'position':'relative',
			'top': ($(window).height() - $('.footer.iphone').outerHeight())/2 - k + 'px'
		});

		$('#prices-footer').css({
			'position': 'relative',
			'top': 40 + 'px'
		});

	}
	set_iphone_footer();
	$(window).resize(function() {
		set_iphone_footer();
	});
	$(window).resize();

	/* pagination */
	$(window).resize(function() {
		$('#pagination').css({
			'position':'relative',
			'left':($(window).width() - $('#pagination').outerWidth())/2 + 'px'
		});
	});
	$(window).resize();


	/* Показать услуги */
	$('span.show').click(function(){
		$(this).parent('.look-detail').children('.hd').toggleClass('active');
		var text = ($(this).parent('.look-detail').children('span.show').text() == 'Скрыть') ? 'Показать услуги' : 'Скрыть';
		$(this).text(text);
	});

	$(window).resize(function(){
		$('.slideContent-List').find('.details-info .detail').css({
			'position' :'relative',
			'left':($(window).width() - $('.slideContent-List').find('.details-info .detail').outerWidth())/2
		});

		if($('.bd').length > 0)
		{
			var width = $('.bd').width();
			var i = $('ul.rate li').index($('ul.rate li.active-list'));
			var triangle_positions = new Array();

			if(width >= 900)
			{
				triangle_positions[0] = 140;
				triangle_positions[1] = 440;
				triangle_positions[2] = 740;
			} else if(width < 900) {
				triangle_positions[0] = 100;
				triangle_positions[1] = 320;
				triangle_positions[2] = 540;
			}

			$('.details-info').css('width', width);
			$('#details-wrap').css({ left: -((width+150)*i) });
			$('#triangle').css({ left: triangle_positions[i]+'px' });
		}
	});
	$(window).resize();


	/* Prices */
	function calc_price(s_price, list)
	{
		var total_price = parseInt($(s_price).attr('base_price'));

		$(list).find('input:checked').each(function(){
			console.log(total_price+' + '+$(this).attr('price'));
			total_price += parseInt($(this).attr('price'));
		});

		//total_price += parseInt($(s_price).attr('base_price'));

		$(s_price).text(total_price.toFixed().replace(/./g, function(c, i, a) {
			return i && c !== "." && !((a.length - i) % 3) ? ' ' + c : c;
		}));
	}

	$('ul._add li input[type="checkbox"]').click(function(){
		var i_section = $('.details-info').index($(this).parents('.details-info'));
		var i = $('.details-info:eq('+i_section+') ul._add').index($(this).parents('ul._add'));
		var s_price = $(this).parents('.details-info').find('.total-price:eq('+i+')');
		var list = $(this).parents('ul._add');

		if($(list).find('input[type="checkbox"]').length !== $(list).find('input[type="checkbox"]:checked').length)
		{
			$('.details-info:eq('+i_section+') input.additional:eq('+i+')').removeAttr('checked').addClass('allChecked');
		} else {
			$('.details-info:eq('+i_section+') input.additional:eq('+i+')').prop('checked', true).removeClass('allChecked');
		}

		if($(list).find('input[type="checkbox"]:checked').length == 0)
		{
			$('.details-info:eq('+i_section+') input.additional:eq('+i+')').removeAttr('checked').removeClass('allChecked');
		}

		calc_price(s_price, list);
	});

	$('input.additional').click(function(){
		var i_section = $('.details-info').index($(this).parents('.details-info'));
		var i = $('.details-info:eq('+i_section+') input.additional').index(this);
		var s_price = $(this).parents('.details-info').find('.total-price:eq('+i+')');
		var list = $('.details-info:eq('+i_section+') ul._add:eq('+i+')');

		$(this).removeClass('allChecked');

		if($(this).is(':checked'))
		{
			$(list).find('input[type="checkbox"]').prop('checked',true);
		} else {
			$(list).find('input[type="checkbox"]').removeAttr('checked');
		}

		calc_price(s_price, list);
	});

	// Contacts
	function init_map()
	{
		var map_canvas = document.getElementById('contacts-map');
		var map_options = {
			center: new google.maps.LatLng(55.749585, 37.5372791),
			zoom: 16,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			mapTypeControlOptions: {
				style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
				position: google.maps.ControlPosition.BOTTOM_RIGHT
			},
			zoomControl: true,
			zoomControlOptions: {
				style: google.maps.ZoomControlStyle.SMALL,
				position: google.maps.ControlPosition.LEFT_CENTER
			},
			panControl: false
		}
		var map = new google.maps.Map(map_canvas, map_options)
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(55.749585, 37.5372791),
			map: map,
			icon: 'http://siteexpo.ru/assets/img/float.png'
		});

	}

	if($('#contacts-map').length > 0)
	{
		google.maps.event.addDomListener(window, 'load', init_map);
	}
});
