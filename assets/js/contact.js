// Contact form
// uses jquery
!function ($) {
	$(function(){
		// Send request
		$('#contact-form').submit(function(){
			$('#contact-form button').animate({ opacity: 0.5 }, 200, function(){ $('#contact-form button').text('Заявка отправляется') }).attr('disabled','disabled').addClass('disabled').removeClass('send-success');

			$.post($('#contact-form').attr('action'), $('#contact-form').serialize(), function(data){
				$('#contact-success').hide();
				$('#valid-mail').hide();
				$('#contact-form .field-error').removeClass('field-error');

				var response = $.parseJSON(data);

				if(response.errors)
				{
					// Empty fields
					for(var n in response.errors)
					{
						$('#contact-form input[name="'+n+'"], #contact-form textarea[name="'+n+'"]').addClass('field-error');
					}

					// Invalid e-mail
					if(response.errors['email'] == 'not_valid')
					{
						$('#valid-mail').show();
					}

					$('#contact-errors').show();
					$('#contact-form button').animate({ opacity: 1 }, 200, function(){ $('#contact-form button').text('Отправить заявку') }).removeAttr('disabled').removeClass('disabled');
				} else if(response.sent && response.sent == 1) {
					yaCounter24951449.reachGoal('request');
					$('#contact-errors').hide();

					if($('#contact-loader').length > 0)
					{
						$('#contact-loader').show();
						$('#contact-loader').animate({ width: 100+'%' }, 3000, function(){
							$('#contact-loader').fadeOut(function(){
								$('#contact-loader').css('width','1px');
							});
							$('#contact-form button').removeClass('disabled').addClass('send-success').html('<img src="/assets/img/submitCheck.png">');
							setTimeout(function(){
								$('#contact-form button').animate({ opacity: 1 }, 200, function(){
									$('#contact-form button').removeAttr('disabled').removeClass('send-success');
								}).text('Отправить заявку');
								$('#contact-form input').val('');
								$('#contact-form textarea').val('');
							}, 2000);
						});
					} else {
						$('#contact-form button').removeClass('disabled').addClass('send-success').html('<img src="/assets/img/submitCheck.png">');
						$('#contact-form button').animate({ opacity: 1 }, 200, function(){
							$('#contact-form button').removeAttr('disabled').removeClass('send-success');
						}).text('Отправить заявку');
						$('#contact-form input').val('');
						$('#contact-form textarea').val('');

						var popWin = $('.modalWin');
						var popBg = $('.modalBg');
						var body = $('body');
						var btn = $('.zakaz > div > button');

						$('#modal-request').removeClass('flipped');
						btn.removeClass('active');
						body.css({'overflow':'auto'});
						popWin.fadeOut(150);
						popBg.fadeOut(150);
					}
				}
			});

			return false;
		});
	})
}(window.jQuery)