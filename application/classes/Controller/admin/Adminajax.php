<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Adminajax extends Controller_Template
{
	# Layout controller
	# ---------------------------------------------------------------------------------------------- #
	public $template = 'admin/ajax';

	public function before()
	{
		parent::before();

		# Смотрим, залогинен ли юзер
		$auth = Auth::instance();

		if($this->request->controller() !== 'Recovery')
		{
			if($auth->logged_in())
			{
				$this->user = Auth::instance()->get_user();
			}
			# Если нет, то просим его авторизоваться
			else
			{
				if($this->request->controller() !== 'auth' && $this->request->action() !== 'login')
				{
					Controller::redirect('/admin/login');
				}
			}
		}
		else
		{
			$this->user = ($auth->logged_in()) ? Auth::instance()->get_user() : FALSE;
		}
	}

	public function after()
	{
		parent::after();
	}
}
?>