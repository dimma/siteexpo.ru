<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Ajax extends Controller_Admin_Adminajax {

	public function action_index()
	{
		echo '==';
		exit;
	}

	// Add service
	public function action_add_service()
	{
		if($this->request->post())
		{
			$post = $this->request->post();
			$post['sort_order'] = ORM::factory('service')->where('category','=',$post['category'])->and_where('type','=',$post['type'])->and_where('package','=',$post['package'])->count_all() + 1;
			$post['date'] = Date::formatted_time();

			if(!is_numeric($post['price']))
			{
				unset($post['price']);
			}

			if($post['name'] !== '')
			{
				$service = ORM::factory('service')->values($post)->save();
				echo $service->id;
			}
		}

		exit;
	}

	// Delete service
	public function action_delete_service()
	{
		if($this->request->post('id'))
		{
			ORM::factory('service', $this->request->post('id'))->delete();
		}

		exit;
	}

	// Edit service
	public function action_edit_service()
	{
		if($this->request->post('service_id'))
		{
			$post = $this->request->post();
			$post['price'] = (isset($post['price']) && is_numeric($post['price'])) ? $post['price'] : 0;

			ORM::factory('service', $post['service_id'])->values($post)->update();
		}

		exit;
	}

	// Base service price
	public function action_set_base_price()
	{
		if($this->request->post('value'))
		{
			$post = $this->request->post();
			$post['value'] = str_replace(' ','',$post['value']);
			ORM::factory('setting', array('name' => $post['name']))->values(array('value' => $post['value']))->update();
		}
		exit;
	}

	// Archive request
	public function action_archive_request()
	{
		if($this->request->post())
		{
			Kohana::ar($this->request->post());
			ORM::factory('request', $this->request->post('request_id'))->values(array('status' => '1'))->update();
		}

		exit;
	}

	// Request info
	public function action_request_info()
	{
		if($this->request->post())
		{
			$data['request'] = ORM::factory('request', $this->request->post('id'));
			$data['labels'] = array('shop' => 'Интернет магазин', 'corporate' => 'Корпоративный', 'startup' => 'Стартап');
			$data['stars_count'] = array(1 => 3, 2 => 4, 3 => 5);

			echo View::factory('admin/requests/_info', $data);
		}

		exit;
	}

}
