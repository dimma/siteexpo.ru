<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Layout extends Controller_Template
{
	# Layout controller
	# ---------------------------------------------------------------------------------------------- #
	public $template = 'layout';

	public function before()
	{
		parent::before();

		# Параметры шаблона
		if($this->auto_render)
		{
			//$detect = new MobileDetect();

			$this->template->title				= '';
			$this->template->window_title		= '';
			$this->template->meta_keywords		= '';
			$this->template->meta_description	= '';
			$this->template->meta_copyright		= '';
			$this->template->meta_author		= '';
			$this->template->styles				= array();
			$this->template->scripts			= array();
			$this->template->controller			= UTF8::strtolower($this->request->controller());
			$this->template->action				= $this->request->action();
			$this->template->main_page			= ($this->request->controller() == 'main' && $this->request->action() == 'index') ? TRUE : FALSE;
			$this->template->content			= '';
			$this->template->menu				= array();
			$this->template->icon               = '';
			$this->template->rights				= array();
			$this->template->detect				= new MobileDetect();
			$this->template->body_class			= '';
			$this->template->android			= FALSE;
			$this->template->map_link			= ($this->template->detect->is('iPhone') || $this->template->detect->is('iPad')) ? 'http://maps.apple.com/?ll=55.749893,37.5372362' : 'http://www.google.ru/maps/place/%D0%9F%D1%80%D0%B5%D1%81%D0%BD%D0%B5%D0%BD%D1%81%D0%BA%D0%B0%D1%8F+%D0%BD%D0%B0%D0%B1.,+12,+%D0%9C%D0%BE%D1%81%D0%BA%D0%B2%D0%B0/@55.749893,37.5372362,17z/data=!3m1!4b1!4m2!3m1!1s0x46b54bdc2341dfc5:0x624eda2900c434bd';

			/* Mobile devices */
			if($this->template->detect->isMobile() || $this->template->detect->isTablet())
			{
				$this->template->body_class = 'mobile';
			} else {
				$this->template->body_class = 'desktop';
			}


			/* iPhone */
			if($this->template->detect->is('iPhone'))
			{
				$this->template->body_class .= ' iphone';
			}

			if($this->template->detect->is('iPad'))
			{
				$this->template->body_class .= ' ipad';

				if($this->template->controller == 'main' && $this->template->action == 'index')
				{
					$this->template->body_class .= ' main';
				}
			}

			/* Android */
			if(stripos(strtolower(Request::$user_agent),'android') !== false)
			{
				$this->template->body_class .= ' android';
				$this->template->android = TRUE;
			}
		}
	}

	public function after()
	{
		if($this->auto_render)
		{
			# Подключаем стандартные стили и скрипты
			$styles['assets/css/reset.css'] = 'screen';
			$styles['assets/css/media-queries.css'] = 'screen';
			$styles['assets/fonts/stylesheet.css'] = 'screen';
			$styles['assets/css/contact.css'] = 'screen';

			if($this->template->detect->is('iPhone'))
			{
				$styles['assets/css/m-style.css'] = 'screen';
			}
			else
			{
				$styles['assets/css/style.css?v=2'] = 'screen';
			}

			$scripts[] = 'assets/js/jquery-2.0.2.min.js';
			$scripts[] = 'assets/js/jquery-ui-1.10.4.custom.js';

			# iPhone swipe
			if($this->template->controller == 'main' && $this->template->action == 'prices')
			{
				$scripts[] = 'assets/js/jquery.touchSwipe.min.js';
			}

			if($this->template->controller == 'main' && $this->template->action == 'contacts')
			{
				$scripts[] = 'https://maps.googleapis.com/maps/api/js?sensor=false';
			}

			$scripts[] = 'assets/js/js.js';
			$scripts[] = 'assets/js/contact.js';

			$this->template->styles		= array_merge($this->template->styles, $styles);
			$this->template->scripts	= array_merge($this->template->scripts, $scripts);
		}

		parent::after();
	}
}
?>