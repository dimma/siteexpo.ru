<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Main extends Controller_Layout {

	# Main page
	public function action_index()
	{
		$data['detect'] = $this->template->detect;
		$data['map_link'] = $this->template->map_link;
		$this->template->title = 'Создание сайтов и мобильных приложений — Сайт Экспо';
		$this->template->content = View::factory('front/main', $data);
	}

	# Prices
	public function action_prices()
	{
		$services = ORM::factory('service')->find_all()->as_array();

		//$data['base_price'] = DB::select('name','value')->from('settings')->execute()->as_array('name');
		$data['base_prices']['shop'][1] = DB::select(DB::expr('SUM(price) as sum'))->from('services')->where('category','=','shop')->and_where('type','=','1')->and_where('package','=','base')->execute()->as_array();
		$data['base_prices']['shop'][2] = DB::select(DB::expr('SUM(price) as sum'))->from('services')->where('category','=','shop')->and_where('type','=','2')->and_where('package','=','base')->execute()->as_array();
		$data['base_prices']['shop'][3] = DB::select(DB::expr('SUM(price) as sum'))->from('services')->where('category','=','shop')->and_where('type','=','3')->and_where('package','=','base')->execute()->as_array();

		$data['base_prices']['corporate'][1] = DB::select(DB::expr('SUM(price) as sum'))->from('services')->where('category','=','corporate')->and_where('type','=','1')->and_where('package','=','base')->execute()->as_array();
		$data['base_prices']['corporate'][2] = DB::select(DB::expr('SUM(price) as sum'))->from('services')->where('category','=','corporate')->and_where('type','=','2')->and_where('package','=','base')->execute()->as_array();
		$data['base_prices']['corporate'][3] = DB::select(DB::expr('SUM(price) as sum'))->from('services')->where('category','=','corporate')->and_where('type','=','3')->and_where('package','=','base')->execute()->as_array();

		$data['base_prices']['startup'][1] = DB::select(DB::expr('SUM(price) as sum'))->from('services')->where('category','=','startup')->and_where('type','=','1')->and_where('package','=','base')->execute()->as_array();
		$data['base_prices']['startup'][2] = DB::select(DB::expr('SUM(price) as sum'))->from('services')->where('category','=','startup')->and_where('type','=','2')->and_where('package','=','base')->execute()->as_array();
		$data['base_prices']['startup'][3] = DB::select(DB::expr('SUM(price) as sum'))->from('services')->where('category','=','startup')->and_where('type','=','3')->and_where('package','=','base')->execute()->as_array();

		$data['base_price']['shop_budget']['value'] = $data['base_prices']['shop'][1][0]['sum'];
		$data['base_price']['shop_standart']['value'] = $data['base_prices']['shop'][2][0]['sum'];
		$data['base_price']['shop_unique']['value'] = $data['base_prices']['shop'][3][0]['sum'];
		$data['base_price']['corporate_budget']['value'] = $data['base_prices']['corporate'][1][0]['sum'];
		$data['base_price']['corporate_standart']['value'] = $data['base_prices']['corporate'][2][0]['sum'];
		$data['base_price']['corporate_unique']['value'] = $data['base_prices']['corporate'][3][0]['sum'];
		$data['base_price']['startup_budget']['value'] = $data['base_prices']['startup'][1][0]['sum'];
		$data['base_price']['startup_standart']['value'] = $data['base_prices']['startup'][2][0]['sum'];
		$data['base_price']['startup_unique']['value'] = $data['base_prices']['startup'][3][0]['sum'];

		$classes = array('1' => 'budget', '2' => 'standart', '3' => 'unique');

		foreach($services as $service)
		{
			if(!isset($data['price_max'][$service->category][$service->type]))
			{
				//$data['price_max'][$service->category][$service->type] = $data['base_price'][$service->category.'_'.$classes[$service->type]]['value'];
				$data['price_max'][$service->category][$service->type] = 0;
			}

			$data['price_max'][$service->category][$service->type] += $service->price;
			$data['services'][$service->category][$service->package][$service->type][$service->id] = $service;
		}

//		Kohana::ar($data['price_max']);

//		die();

		$data['map_link'] = $this->template->map_link;

		$template = ($this->template->detect->is('iPhone')) ? 'prices_iphone' : 'prices';
		$this->template->title = 'Разработка и дизайн сайтов для бизнеса — Сайт Экспо';
		$this->template->content = View::factory('front/'.$template, $data);
	}

	# Portfolio
	public function action_portfolio()
	{
		$data['map_link'] = $this->template->map_link;
		$this->template->title = 'Портфолио | Сайт Экспо';
		$this->template->content = View::factory('front/portfolio');
	}

	# Contacts
	public function action_contacts()
	{
		$data['map_link'] = $this->template->map_link;
		$template = ($this->template->detect->is('iPhone')) ? 'contacts_iphone' : 'contacts';
		$this->template->title = 'Контакты | Сайт Экспо';
		$this->template->content = View::factory('front/'.$template, $data);
	}

}
