<div class="panel">
	<nav id="side-nav" class="side-navigation">
		<ul>
			<li><a class="active" href="#main" data-link="1"><span class="pag"></span><span class="hover-text">Главная</span></a></li>
			<li><a href="#portfolio" data-link="2"><span class="pag"></span><span class="hover-text">Портфолио</span></a></li>
			<li><a href="#disposal" data-link="3"><span class="pag"></span><span class="hover-text">Услуги</span></a></li>
			<li><a href="#team" data-link="4"><span class="pag"></span><span class="hover-text">Команда</span></a></li>
			<li><a href="#contacts" data-link="5"><span class="pag"></span><span class="hover-text">Контакты</span></a></li>
		</ul>
	</nav>
</div>

<section id="preview" data-section="1">
	<div class="topNav" id="nav">
		<a href="/"><? if($detect->is('iPhone')) { ?><img src="/assets/img/logofix.svg" alt=""><? } else { ?><img src="/assets/img/logo.svg" alt=""><? } ?></a>
		<nav>
			<ul>
				<li><a class="active" href="/"><span>Главная</span></a></li>
				<li><a href="/prices"><span>Цены</span></a></li>
				<li><a href="/contacts"><span>Контакты</span></a></li>
				<div class="clearfix"></div>
			</ul>
		</nav>
		<h3>+7 (495) 374-64-82</h3>
	</div>

	<div class="topNavMob">
		<span></span>
	</div>
	<div id="menu">
		<ul>
			<li><a class="active" href="/"><span>Главная</span></a></li>
			<li><a href="/prices"><span>Цены</span></a></li>
			<li><a href="/contacts"><span>Контакты</span></a></li>
			<li><a href="tel:+7 (495) 374-64-82">+7 (495) 374-64-82</a></li>
		</ul>
	</div>

	<div class="wrapper">
		<div class="middle-block">
			<h1>Хороший сайт&nbsp;&mdash; это часть успешного бизнеса.</h1>
			<a href="/prices"><button id="count">Заказать сайт</button></a>
			<div class="clearfix"></div>
		</div>
		<a class="move" href="#portfolio" data-link="2"><img src="/assets/img/move.svg" alt=""></a>
	</div>
</section>

<section id="portfolio" name="portfolio" data-section="2">
	<div class="topNav fix" id="secondary-nav">
		<div class="nav-content">
			<a class="logo" href="/"><img src="/assets/img/logofix.svg" alt=""></a>
			<nav>
				<ul>
					<li><a class="active" href="/" title="Главная"><span>Главная</span></a></li>
					<li><a href="/prices"><span>Цены</span></a></li>
					<li><a href="/contacts" title="Портфолио"><span>Контакты</span></a></li>
					<div class="clearfix"></div>
				</ul>
			</nav>
			<h3><a href="tel:+7 (495) 374-64-82">+7 (495) 374-64-82</a></h3>
		</div>
		<div id="contact-loader"></div>
	</div>

	<div class="middle-block-portfolio">
		<img src="/assets/img/logoRoomb.png" alt="">

		<div class="link">
			<h1>Персональное облако</h1>
			<a href="http://www.orbios.com/" target="_blank">orbios.com</a>
		</div>
	</div>
	<div class="portfolioBg"></div>
	<div class="description">
				<span>
					<p>Мы разрабатываем сайты, которые увеличивают ваши продажи.</p>
					<!--<button id="show_more">Все проекты</button>-->
				</span>
	</div>
</section>

<section id="disposal" name="disposal" data-section="3">
	<div class="wrapper">
		<div class="middle-block-disposal">
			<h1>Услуги</h1>
			<ul>
				<li class="anim-disp">
					<img src="/assets/img/L-1.svg" alt="">

					<div>
						<h2>Дизайн и разработка</h2>

						<h3>Мы разрабатываем сайты для интернет-магазинов, сайты-визитки, имиджевые и промо сайты,
							корпоративные информационные сайты. Наши дизайнеры создадут для вас современный дизайн сайта
							и подскажут, какие решения сейчас в тренде.</h3>
					</div>
				</li>
				<li class="anim-disp">
					<img src="/assets/img/L-2.svg" alt="">

					<div>
						<h2>Мобильные приложения</h2>

						<h3>Более 40% пользователей заходят в интернет через смартфон или планшет. Собственное
							приложение — это не только следование современным тенденциям, но и удобный инструмент для
							клиентов. Наши приложения работают стабильно на всех современных мобильных устройствах.</h3>
					</div>
				</li>
				<li class="anim-disp">
					<img src="/assets/img/L-3.svg" alt="">

					<div>
						<h2>Продвижение сайта</h2>

						<h3>Для того чтобы сайт начал приносить прибыль, он должен быть эффективным. Мы предлагаем комплексные решения продвижения в поисковых системах, раскрутку в социальных сетях и проведение специальных маркетинговых мероприятий.</h3>
					</div>
				</li>
				<li class="anim-disp">
					<img src="/assets/img/L-4.svg" alt="">

					<div>
						<h2>Фирменный стиль</h2>

						<h3>Если вы хотите, чтобы ваш бренд был узнаваем, вам нужен запоминающийся фирменный стиль. Мы
							разработаем для вас логотип, придумаем название для бренда, предложим готовые решения для
							рекламы, оформления деловой документации и упаковки.</h3>
					</div>
				</li>
			</ul>
				<a href="/prices"><button id="booking">Заказать</button></a>
			<div class="clearfix"></div>
		</div>
	</div>
</section>

<section id="team" name="team" data-section="4">
	<div class="wrapper">
		<div class="middle-block-team">
			<h1>Команда</h1>

			<h3>Для нас новый проект - это вызов нашим способностям и умениям.</h3>
			<h3>Мы ищем оригинальные решения, которые подойдут именно вашему
				бизнесу.</h3>
			<ul>
				<li>
					<div class="foto">
						<img src="/assets/img/team/f-1.png" alt="">
					</div>
					<div class="title">
						<div>
							<h2>Мугутдин</h2>
							<h3>Генеральный директор</h3>
						</div>
					</div>
				</li>
				<li>
					<div class="foto">
						<img src="/assets/img/team/f-11.png" alt="">
					</div>
					<div class="title">
						<div>
							<h2>Кирилл</h2>
							<h3>Менеджер проектов</h3>
						</div>
					</div>
				</li>
				<li>
					<div class="foto">
						<img src="/assets/img/team/f-2.png" alt="">
					</div>
					<div class="title">
						<div>
							<h2>Милош</h2>
							<h3>Разработчик</h3>
						</div>
					</div>
				</li>
				<li>
					<div class="foto">
						<img src="/assets/img/team/f-3.png" alt="">
					</div>
					<div class="title">
						<div>
							<h2>Дмитрий</h2>
							<h3>Арт-директор</h3>
						</div>
					</div>
				</li>
				<li>
					<div class="foto">
						<img src="/assets/img/team/f-4.png" alt="">
					</div>
					<div class="title">
						<div>
							<h2>Евгений</h2>
							<h3>Проектировщик</h3>
						</div>
					</div>
				</li>

				<li>
					<div class="foto">
						<img src="/assets/img/team/f-5.png" alt="">
					</div>
					<div class="title">
						<div>
							<h2>Артём</h2>
							<h3>Копирайтер</h3>
						</div>
					</div>
				</li>
				<li>
					<div class="foto">
						<img src="/assets/img/team/f-6.png" alt="">
					</div>
					<div class="title">
						<div>
							<h2>Николай</h2>
							<h3>Технолог</h3>
						</div>
					</div>
				</li>
				<li>
					<div class="foto">
						<img src="/assets/img/team/f-10.png" alt="">
					</div>
					<div class="title">
						<div>
							<h2>Вадим</h2>
							<h3>Программист</h3>
						</div>
					</div>
				</li>
				<li>
					<div class="foto">
						<img src="/assets/img/team/f-7.png" alt="">
					</div>
					<div class="title">
						<div>
							<h2>Максим</h2>
							<h3>Дизайнер</h3>
						</div>
					</div>
				</li>
				<li>
					<div class="foto">
						<img src="/assets/img/team/f-8.png" alt="">
					</div>
					<div class="title">
						<div>
							<h2>Рустам</h2>
							<h3>Программист</h3>
						</div>
					</div>
				</li>
				<li>
					<div class="foto">
						<img src="/assets/img/team/f-9.png" alt="">
					</div>
					<div class="title">
						<div>
							<h2>Наталья</h2>
							<h3>СЕО-специалист</h3>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
</section>

<section id="contacts" name="contacts" data-section="5">
	<div class="wrapper">
		<div class="middle-block-contacts">
			<div class="contacts-info">
				<ul>
					<li><h2>Контакты</h2>

						<div><span><a href="tel:+7 (495) 374-64-82">+7 (495) 374-64-82</a></span>
							<p><a href="mailto:info@siteexpo.ru?Subject=Zakaz">info@siteexpo.ru</a></p>
							<br>
							<p><span>Москва, Пресненская набережная, д. 12 ММДЦ «Москва Сити», комплекс Федерация, башня Восток, 31 этаж, офис Q</span></p>
							<br>
							<p><a href="<?=$map_link?>" target="_blank">Посмотреть на карте</a></p>
						</div>
					</li>
					<!--<li><h2>Мы в соцсетях</h2>

						<div>
							<a href="#"><img src="/assets/img/media/_facebook.svg" alt=""></a>
							<a href="#"><img src="/assets/img/media/_VK.svg" alt=""></a>
							<a href="#"><img src="/assets/img/media/_linkdin.svg" alt=""></a>
							<a href="#"><img src="/assets/img/media/_instagram.svg" alt=""></a>
							<a href="#"><img src="/assets/img/media/_behance.svg" alt=""></a>
							<a href="#"><img src="/assets/img/media/_dribbble.svg" alt=""></a>
						</div>
					</li>-->
				</ul>
			</div>
			<div class="sidebar">
				<h2>Связаться с нами</h2>

				<div class="form_input">
					<form action="/ajax/contact" method="post" accept-charset="utf-8" id="contact-form">
						<input type="hidden" name="request" value="1">

						<div class="inpt">
							<p><input type="text" name="name" placeholder="Имя"></p>
							<p><input type="text" name="phone" placeholder="Телефон"></p>
							<p><input type="text" name="email" placeholder="Почта"></p>
							<textarea name="text" placeholder="Сообщение"></textarea>
						</div>
						<p class="contact-required txt">Поля «Имя» и «Телефон» обязательны для заполнения</p>
						<button id="send" class="contact-btn">Отправить заявку</button>
					</form>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="copyright"><p>ООО «СайтЭкспо», XSoft ltd. &copy; 2014</p></div>
</section>