<div class="wrapper price contacts">

	<div class="_map" id="contacts-map"></div>

	<div class="topNav price contacts" id="nav">
		<a href="/"><img src="/assets/img/logo.svg" alt=""></a>
		<nav>
			<ul>
				<li><a href="/"><span>Главная</span></a></li>
				<li><a href="/prices"><span>Цены</span></a></li>
				<li><a class="active" href="/contacts"><span>Контакты</span></a></li>
			</ul>
		</nav>
		<div>
			<h3>+7 (495) 374-64-82</h3>
			<p><a href="mailto:info@siteexpo.ru?Subject=Zakaz">info@siteexpo.ru</a></p>
		</div>
	</div>

	<div class="contacts-contact">

		<div class="location">
			<p>Москва, Пресненская набережная, д. 12<br> ММДЦ «Москва Сити», комплекс Федерация, башня Восток, 31 этаж, офис Q</p>
		</div>

		<div class="media">
			<p>ООО «СайтЭкспо», XSoft ltd. &copy; 2014</p>
			<!--<a href="#"><img src="/assets/img/media/facebook-price.svg" alt=""></a>
			<a href="#"><img src="/assets/img/media/VK-price.svg" alt=""></a>
			<a href="#"><img src="/assets/img/media/linkdin-price.svg" alt=""></a>
			<a href="#"><img src="/assets/img/media/instagram-price.svg" alt=""></a>
			<a href="#"><img src="/assets/img/media/behance-price.svg" alt=""></a>
			<a href="#"><img src="/assets/img/media/dribbble-price.svg" alt=""></a>-->
		</div>
	</div>

	<div class="topNavMob">
		<span></span>
	</div>
	<div id="menu">
		<ul>
			<li><a href="/"><span>Главная</span></a></li>
			<li><a href="/prices"><span>Цены</span></a></li>
			<li><a class="active" href="/contacts"><span>Контакты</span></a></li>
			<li><a href="tel:+7 (495) 374-64-82">+7 (495) 374-64-82</a></li>
		</ul>
	</div>

</div>