<div class="wrapper price">
	<div class="topNav price" id="nav">
		<a href="/"><img src="/assets/img/logo.svg" alt=""></a>
		<nav>
			<ul>
				<li><a href="/"><span>Главная</span></a></li>
				<li><a class="active" href="/prices"><span>Цены</span></a></li>
				<li><a href="/contacts"><span>Контакты</span></a></li>
			</ul>
		</nav>
		<div>
			<h3>+7 (495) 374-64-82</h3>
			<p>Позвоните в отдел продаж</p>
		</div>
	</div>

	<div class="topNavMob">
		<span></span>
	</div>
	<div id="menu">
		<ul>
			<li><a href="/"><span>Главная</span></a></li>
			<li><a class="active" href="/prices"><span>Цены</span></a></li>
			<li><a href="/contacts"><span>Контакты</span></a></li>
			<li><a href="tel:+7 (495) 374-64-82">+7 (495) 374-64-82</a></li>
		</ul>
	</div>

	<div class="selection">
		<h1>Какой сайт вы хотите?</h1>
		<ul class="rate">
			<li>
				<div class="fotoimg-wrap">
					<div class="fotoImg left"></div>
					<div class="fotoImg left active-img"></div>
				</div>
				<h2>Интернет-магазин</h2>
				<h3>Актуальный и эффективный инструмент продажи товаров и услуг через интернет.</h3>
			</li>
			<li>
				<div class="fotoimg-wrap">
					<div class="fotoImg middle"></div>
					<div class="fotoImg middle active-img"></div>
				</div>
				<h2>Корпоративный</h2>
				<h3>Корпоративный сайт — важная имиджевая составляющая любого бизнеса.</h3>
			</li>
			<li>
				<div class="fotoimg-wrap">
					<div class="fotoImg right"></div>
					<div class="fotoImg right active-img"></div>
				</div>
				<h2>Стартап</h2>
				<h3>Идеально подходит для крупных проектов с большим количеством пользователей.</h3>
			</li>
		</ul>
	</div>

	<div class="clearfix"></div>

	<div class="info iphone" id="info">
		<div class="bd">
			<div class="triangle" id="triangle"></div>
			<div class="bd-wrap">
				<div id="details-wrap">
					<div id="tab1" class="details-info">
						<div class="detail">
							<h2>Бюджетный</h2>
							<i><?=Num::format($base_price['shop_budget']['value'], 0, ',', ' ')?>&ndash;<?=Num::format($price_max['shop'][1], 0, ',', ' ')?> <span>a</span></i>
							<div class="stars">
								<span class="star s-3"></span>
							</div>
							<div class="list">
								<h3>Базовые функции</h3>
								<ul>
									<? foreach($services['shop']['base'][1] as $service) { ?>
										<li><?=$service->name?></li>
									<? } ?>
								</ul>
								<h3>
									<input type="checkbox" class="additional" id="title_1">
									<label for="title_1">Дополнительно</label>
								</h3>
								<ul class="_add">
									<? foreach($services['shop']['additional'][1] as $service) { ?>
										<li>
											<input type="checkbox" service_id="<?=$service->id?>" id="add_<?=$service->id?>" price="<?=$service->price?>">
											<label for="add_<?=$service->id?>"><?=$service->name?></label>
										</li>
									<? } ?>
								</ul>
							</div>
							<div class="result">
								<h3>Итого: <span class="total-price" base_price="<?=$base_price['shop_budget']['value']?>"><?=Num::format($base_price['shop_budget']['value'], 0, ',', ' ')?></span> <span>a</span></h3>
								<p>Цена на создание бюджетного интернет-магазина. Все цены указаны в рублях.</p>
							</div>
						</div>
						<div class="detail">
							<h2>Стандартный</h2>
							<i><?=Num::format($base_price['shop_standart']['value'], 0, ',', ' ')?>&ndash;<?=Num::format($price_max['shop'][2], 0, ',', ' ')?> <span>a</span></i>
							<div class="stars">
								<span class="star s-4"></span>
							</div>
							<div class="list">
								<h3>Базовые функции</h3>
								<ul>
									<? foreach($services['shop']['base'][2] as $service) { ?>
										<li><?=$service->name?></li>
									<? } ?>
								</ul>
								<h3>
									<input type="checkbox" class="additional" id="title_2">
									<label for="title_2">Дополнительно</label>
								</h3>
								<ul class="_add">
									<? foreach($services['shop']['additional'][2] as $service) { ?>
										<li>
											<input type="checkbox" service_id="<?=$service->id?>" id="add_<?=$service->id?>" price="<?=$service->price?>">
											<label for="add_<?=$service->id?>"><?=$service->name?></label>
										</li>
									<? } ?>
								</ul>
							</div>
							<div class="result">
								<h3>Итого: <span class="total-price" base_price="<?=$base_price['shop_standart']['value']?>"><?=Num::format($base_price['shop_standart']['value'], 0, ',', ' ')?></span> <span>a</span></h3>
								<p>Цена на создание стандартного интернет-магазина. Все цены указаны в рублях.</p>
							</div>
						</div>
						<div class="detail">
							<h2>Уникальный</h2>
							<i><?=Num::format($base_price['shop_unique']['value'], 0, ',', ' ')?>&ndash;<?=Num::format($price_max['shop'][3], 0, ',', ' ')?> <span>a</span></i>
							<div class="stars">
								<span class="star s-5"></span>
							</div>
							<div class="list">
								<h3>Базовые функции</h3>
								<ul>
									<? foreach($services['shop']['base'][3] as $service) { ?>
										<li><?=$service->name?></li>
									<? } ?>
								</ul>
								<h3>
									<input type="checkbox" class="additional" id="title_3">
									<label for="title_3">Дополнительно</label>
								</h3>
								<ul class="_add">
									<? foreach($services['shop']['additional'][3] as $service) { ?>
										<li>
											<input type="checkbox" service_id="<?=$service->id?>" id="add_<?=$service->id?>" price="<?=$service->price?>">
											<label for="add_<?=$service->id?>"><?=$service->name?></label>
										</li>
									<? } ?>
								</ul>
							</div>
							<div class="result">
								<h3>Итого: <span class="total-price" base_price="<?=$base_price['shop_unique']['value']?>"><?=Num::format($base_price['shop_unique']['value'], 0, ',', ' ')?></span> <span>a</span></h3>
								<p>Цена на создание уникального интернет-магазина. Все цены указаны в рублях.</p>
							</div>
						</div>

						<div class="zakaz">
							<div><button class="trans bud" category="shop" type="1">Заказать бюджетный</button></div>
							<div><button class="color st" category="shop" type="2">Заказать стандартный</button></div>
							<div><button class="trans un" category="shop" type="3">Заказать уникальный</button></div>
						</div>
					</div><!-- TAB 1 -->

					<div id="tab2" class="details-info">
						<div class="detail">
							<h2>Бюджетный</h2>
							<i><?=Num::format($base_price['corporate_budget']['value'], 0, ',', ' ')?>&ndash;<?=Num::format($price_max['corporate'][1], 0, ',', ' ')?> <span>a</span></i>
							<div class="stars">
								<span class="star s-3"></span>
							</div>
							<div class="list">
								<h3>Базовые функции</h3>
								<ul>
									<? foreach($services['corporate']['base'][1] as $service) { ?>
										<li><?=$service->name?></li>
									<? } ?>
								</ul>
								<h3>
									<input type="checkbox" class="additional" id="title_4">
									<label for="title_4">Дополнительно</label>
								</h3>
								<ul class="_add">
									<? foreach($services['corporate']['additional'][1] as $service) { ?>
										<li>
											<input type="checkbox" service_id="<?=$service->id?>" id="add_<?=$service->id?>" price="<?=$service->price?>">
											<label for="add_<?=$service->id?>"><?=$service->name?></label>
										</li>
									<? } ?>
								</ul>
							</div>
							<div class="result">
								<h3>Итого <span class="total-price" base_price="<?=$base_price['corporate_budget']['value']?>"><?=Num::format($base_price['corporate_budget']['value'], 0, ',', ' ')?></span> <span>a</span></h3>
								<p>Цена на создание бюджетного корпоративного сайта. Все цены указаны в рублях.</p>
							</div>
						</div>
						<div class="detail">
							<h2>Стандартный</h2>
							<i><?=Num::format($base_price['corporate_standart']['value'], 0, ',', ' ')?>&ndash;<?=Num::format($price_max['corporate'][2], 0, ',', ' ')?> <span>a</span></i>
							<div class="stars">
								<span class="star s-4"></span>
							</div>
							<div class="list">
								<h3>Базовые функции</h3>
								<ul>
									<? foreach($services['corporate']['base'][2] as $service) { ?>
										<li><?=$service->name?></li>
									<? } ?>
								</ul>
								<h3>
									<input type="checkbox" class="additional" id="title_5">
									<label for="title_5">Дополнительно</label>
								</h3>
								<ul class="_add">
									<? foreach($services['corporate']['additional'][2] as $service) { ?>
										<li>
											<input type="checkbox" service_id="<?=$service->id?>" id="add_<?=$service->id?>" price="<?=$service->price?>">
											<label for="add_<?=$service->id?>"><?=$service->name?></label>
										</li>
									<? } ?>
								</ul>
							</div>
							<div class="result">
								<h3>Итого <span class="total-price" base_price="<?=$base_price['corporate_standart']['value']?>"><?=Num::format($base_price['corporate_standart']['value'], 0, ',', ' ')?></span> <span>a</span></h3>
								<p>Цена на создание cтандартного корпоративного сайта. Все цены указаны в рублях.</p>
							</div>
						</div>
						<div class="detail">
							<h2>Уникальный</h2>
							<i><?=Num::format($base_price['corporate_unique']['value'], 0, ',', ' ')?>&ndash;<?=Num::format($price_max['corporate'][3], 0, ',', ' ')?> <span>a</span></i>
							<div class="stars">
								<span class="star s-5"></span>
							</div>
							<div class="list">
								<h3>Базовые функции</h3>
								<ul>
									<? foreach($services['corporate']['base'][3] as $service) { ?>
										<li><?=$service->name?></li>
									<? } ?>
								</ul>
								<h3>
									<input type="checkbox" class="additional" id="title_6">
									<label for="title_6">Дополнительно</label>
								</h3>
								<ul class="_add">
									<? foreach($services['corporate']['additional'][3] as $service) { ?>
										<li>
											<input type="checkbox" service_id="<?=$service->id?>" id="add_<?=$service->id?>" price="<?=$service->price?>">
											<label for="add_<?=$service->id?>"><?=$service->name?></label>
										</li>
									<? } ?>
								</ul>
							</div>
							<div class="result">
								<h3>Итого <span class="total-price" base_price="<?=$base_price['corporate_unique']['value']?>"><?=Num::format($base_price['corporate_unique']['value'], 0, ',', ' ')?></span> <span>a</span></h3>
								<p>Цена на создание уникального корпоративного сайта. Все цены указаны в рублях.</p>
							</div>
						</div>
						<div class="zakaz">
							<div><button class="trans bud" category="corporate" type="1">Заказать бюджетный</button></div>
							<div><button class="color st" category="corporate" type="2">Заказать стандартный</button></div>
							<div><button class="trans un" category="corporate" type="3">Заказать уникальный</button></div>
						</div>

					</div><!-- TAB 2 -->

					<div id="tab3" class="details-info">
						<div class="detail">
							<h2>Бюджетный</h2>
							<i><?=Num::format($base_price['startup_budget']['value'], 0, ',', ' ')?>&ndash;<?=Num::format($price_max['startup'][1], 0, ',', ' ')?> <span>a</span></i>
							<div class="stars">
								<span class="star s-3"></span>
							</div>
							<div class="list">
								<h3>Базовые функции</h3>
								<ul>
									<? foreach($services['startup']['base'][1] as $service) { ?>
										<li><?=$service->name?></li>
									<? } ?>
								</ul>
								<h3>
									<input type="checkbox" class="additional" id="title_7">
									<label for="title_7">Дополнительно</label>
								</h3>
								<ul class="_add">
									<? foreach($services['startup']['additional'][1] as $service) { ?>
										<li>
											<input type="checkbox" service_id="<?=$service->id?>" id="add_<?=$service->id?>" price="<?=$service->price?>">
											<label for="add_<?=$service->id?>"><?=$service->name?></label>
										</li>
									<? } ?>
								</ul>
							</div>
							<div class="result">
								<h3>Итого <span class="total-price" base_price="<?=$base_price['startup_budget']['value']?>"><?=Num::format($base_price['startup_budget']['value'], 0, ',', ' ')?></span> <span>a</span></h3>
								<p>Цена на создание бюджетного стартапа. Все цены указаны в рублях.</p>
							</div>
						</div>
						<div class="detail">
							<h2>Стандартный</h2>
							<i><?=Num::format($base_price['startup_standart']['value'], 0, ',', ' ')?>&ndash;<?=Num::format($price_max['startup'][2], 0, ',', ' ')?> <span>a</span></i>
							<div class="stars">
								<span class="star s-4"></span>
							</div>
							<div class="list">
								<h3>Базовые функции</h3>
								<ul>
									<? foreach($services['startup']['base'][2] as $service) { ?>
										<li><?=$service->name?></li>
									<? } ?>
								</ul>
								<h3>
									<input type="checkbox" class="additional" id="title_8">
									<label for="title_8">Дополнительно</label>
								</h3>
								<ul class="_add">
									<? foreach($services['startup']['additional'][2] as $service) { ?>
										<li>
											<input type="checkbox" service_id="<?=$service->id?>" id="add_<?=$service->id?>" price="<?=$service->price?>">
											<label for="add_<?=$service->id?>"><?=$service->name?></label>
										</li>
									<? } ?>
								</ul>
							</div>
							<div class="result">
								<h3>Итого <span class="total-price" base_price="<?=$base_price['startup_standart']['value']?>"><?=Num::format($base_price['startup_standart']['value'], 0, ',', ' ')?></span> <span>a</span></h3>
								<p>Цена на создание стандартного стартапа. Все цены указаны в рублях.</p>
							</div>
						</div>
						<div class="detail">
							<h2>Уникальный</h2>
							<i><?=Num::format($base_price['startup_unique']['value'], 0, ',', ' ')?>&ndash;<?=Num::format($price_max['startup'][3], 0, ',', ' ')?> <span>a</span></i>
							<div class="stars">
								<span class="star s-5"></span>
							</div>
							<div class="list">
								<h3>Базовые функции</h3>
								<ul>
									<? foreach($services['startup']['base'][3] as $service) { ?>
										<li><?=$service->name?></li>
									<? } ?>
								</ul>
								<h3>
									<input type="checkbox" class="additional" id="title_9">
									<label for="title_9">Дополнительно</label>
								</h3>
								<ul class="_add">
									<? foreach($services['startup']['additional'][3] as $service) { ?>
										<li>
											<input type="checkbox" service_id="<?=$service->id?>" id="add_<?=$service->id?>" price="<?=$service->price?>">
											<label for="add_<?=$service->id?>"><?=$service->name?></label>
										</li>
									<? } ?>
								</ul>
							</div>
							<div class="result">
								<h3>Итого <span class="total-price" base_price="<?=$base_price['startup_unique']['value']?>"><?=Num::format($base_price['startup_unique']['value'], 0, ',', ' ')?></span> <span>a</span></h3>
								<p>Цена на создание уникального стартапа. Все цены указаны в рублях.</p>
							</div>
						</div>
						<div class="zakaz">
							<div><button class="trans bud" category="startup" type="1">Заказать бюджетный</button></div>
							<div><button class="color st" category="startup" type="2">Заказать стандартный</button></div>
							<div><button class="trans un" category="startup" type="3">Заказать уникальный</button></div>
						</div>

					</div><!-- TAB 3 -->
				</div>
			</div>
		</div>
	</div>
	<div class="push"></div>
</div>

<div class="clearfix"></div>



<div class="footer">
	<div class="media">
		<!--<a href="#"><img src="/assets/img/media/facebook-price.svg" alt=""></a>
		<a href="#"><img src="/assets/img/media/VK-price.svg" alt=""></a>
		<a href="#"><img src="/assets/img/media/linkdin-price.svg" alt=""></a>
		<a href="#"><img src="/assets/img/media/instagram-price.svg" alt=""></a>
		<a href="#"><img src="/assets/img/media/behance-price.svg" alt=""></a>
		<a href="#"><img src="/assets/img/media/dribbble-price.svg" alt=""></a>-->
		<p>ООО «СайтЭкспо», XSoft ltd. &copy; 2014</p>
	</div>
	<div class="contacts">
		<p><a href="tel:+7 (495) 374-64-82">+7 (495) 374-64-82</a></p>
		<a href="mailto:info@siteexpo.ru?Subject=Zakaz">info@siteexpo.ru</a>
	</div>
	<div class="location">
		<p>Москва, Пресненская набережная, д. 12<br> ММДЦ «Москва Сити», комплекс Федерация, башня Восток, 31 этаж, офис Q</p>
		<p><a href="<?=$map_link?>" target="_blank">Посмотреть на карте</a></p>
	</div>
</div>

<!-- MODAL WINDOW -->

<div class="modalBg"></div>
<div class="modalWin" id="modal-request">
	<div class="request-wrap">
		<i>Заявка<br> на <span class="site-type"></span> <span class="site-category"></span></i>
		<div class="stars"><span class="star s-5"></span></div>
		<div class="form_input">
			<form action="/ajax/contact" method="post" accept-charset="utf-8" id="contact-form">
				<input type="hidden" name="request" value="1">
				<input type="hidden" name="services">
				<input type="hidden" name="category">
				<input type="hidden" name="type">

				<div class="inpt">
					<p><input type="text" name="name" placeholder="Имя"></p>
					<p><input type="text" name="phone" placeholder="Телефон"></p>
					<p><input type="text" name="email" placeholder="Почта"></p>
				</div>
				<button id="send" class="contact-btn">Отправить заявку</button>
			</form>
		</div>
		<p>Сразу после того, как мы получим вашу заявку, наш менеджер свяжется с вами для уточнения деталей.</p>
		<br>
		<p>Вы можете не заполнять заявку.</p>
		<p>Просто позвоните нам +7 (495) 374-64-82.</p>
	</div>
</div>
<!-- END MODAL WINDOW -->
