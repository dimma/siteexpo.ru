<div class="wrapper price">
	<div class="topNav price" id="nav">
		<a href="/"><img src="/assets/img/logofix.svg" alt=""></a>
		<nav>
			<ul>
				<li><a href="/"><span>Главная</span></a></li>
				<li><a class="active" href="/prices"><span>Цены</span></a></li>
				<li><a href="/contacts"><span>Контакты</span></a></li>
			</ul>
		</nav>
		<div>
			<h3>+7 (495) 374-64-82</h3>
			<p>Позвоните в отдел продаж</p>
		</div>
	</div>

	<div class="topNavMob">
		<span></span>
	</div>
	<div id="menu">
		<ul>
			<li><a href="/"><span>Главная</span></a></li>
			<li><a href="prices/"><span>Цены</span></a></li>
			<li><a class="active" href="/contacts"><span>Контакты</span></a></li>
			<li><a href="tel:+7 (495) 374-64-82">+7 (495) 374-64-82</a></li>
		</ul>
	</div>



<div class="footer iphone" id="contacts-footer">
	<div class="centering">

		<div class="contacts">
			<p><a href="tel:+7 (495) 374-64-82">+7 (495) 374-64-82</a></p>
			<a href="mailto:info@siteexpo.ru?Subject=Zakaz">info@siteexpo.ru</a>
		</div>

		<div class="location">
			<p>Москва, Пресненская набережная, д.&nbsp;12<br> ММДЦ &laquo;Москва Сити&raquo;, комплекс Федерация, башня Восток, 31&nbsp;этаж, офис&nbsp;Q</p>
			<p><a href="<?=$map_link?>" target="_blank">Посмотреть на карте</a></p>
		</div>

		<div class="media">
			<p>ООО «СайтЭкспо», XSoft ltd. &copy; 2014</p>
			<!--<a href="#"><img src="/assets/img/media/facebook-price.svg" alt=""></a>
			<a href="#"><img src="/assets/img/media/VK-price.svg" alt=""></a>
			<a href="#"><img src="/assets/img/media/linkdin-price.svg" alt=""></a>
			<a href="#"><img src="/assets/img/media/instagram-price.svg" alt=""></a>
			<a href="#"><img src="/assets/img/media/behance-price.svg" alt=""></a>
			<a href="#"><img src="/assets/img/media/dribbble-price.svg" alt=""></a>-->
		</div>
	</div>
</div>
</div>