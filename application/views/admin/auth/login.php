<!DOCTYPE html>
<html>
	<head>
		<title>Авторизация | SiteExpo administration area</title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<meta content='text/html;charset=utf-8' http-equiv='content-type'>
		<meta content='Flat administration template for Twitter Bootstrap. Twitter Bootstrap 3 template with Ruby on Rails support.' name='description'>

		<meta name="keywords" content="<?=$meta_keywords;?>" />
		<meta name="description" content="<?=$meta_description;?>" />
		<meta name="copyright" content="<?=$meta_copyright;?>" />
		<meta name="author" content="<?=$meta_author;?>" />
		<? foreach($styles as $file => $type) { echo HTML::style($file, array('media' => $type)), "\n"; } ?>

		<!--[if lt IE 9]>
		<script src="/assets/javascripts/ie/html5shiv.js" type="text/javascript"></script>
		<script src="/assets/javascripts/ie/respond.min.js" type="text/javascript"></script>
		<![endif]-->
	</head>
	<body class='contrast-sea-blue login contrast-background'>
		<div class='middle-container'>
			<div class='middle-row'>
				<div class='middle-wrapper'>
					<div class='login-container-header'>
						<div class='container'>
							<div class='row'>
								<div class='col-sm-12'>
									<div class='text-center'>
										<img width="78" height="89" src="/assets/admin/i/logo_auth.png" />
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class='login-container'>
						<div class='container'>
							<div class='row'>
								<div class='col-sm-4 col-sm-offset-4'>
									<div id="login-box">
										<h1 class='text-center title'>Авторизация</h1>
										<?=Form::open('/admin/login/', array('class'=>'validate-form','id'=>'login-form'))?>
											<div class='form-group'>
												<div class='controls with-icon-over-input'>
													<input value="" placeholder="Логин" class="form-control" data-rule-required="true" name="username" type="text" />
													<i class='icon-user text-muted'></i>
												</div>
											</div>
											<div class='form-group'>
												<div class='controls with-icon-over-input'>
													<input value="" placeholder="Пароль" class="form-control" data-rule-required="true" name="password" type="password" />
													<i class='icon-lock text-muted'></i>
												</div>
											</div>
											<? if(isset($error['login_falied'])) { ?>
												<span class="text-red">Неправильное имя пользователя или пароль.</span>
											<? } ?>
											<div class='checkbox'>
												<label for='remember_me'>
													<input id='remember_me' name='remember_me' type='checkbox' value='1'>
													Запомнить
												</label>
											</div>
											<button class='btn btn-block'>Войти</button>
										<?=Form::close()?>
										<? /*
										<div class='text-center'>
											<hr class='hr-normal'>
											<span class="local" id="forgot-lnk">Забыли пароль?</span>
										</div>
 										*/ ?>
									</div>
									<div id="recovery-box" style="display: none">
										<h2 class='text-center title'>Восстановление пароля</h2>
										<?=Form::open('/recovery/send', array('class'=>'validate-form','id'=>'recovery-form'))?>
										<div class='form-group'>
											<div class='controls with-icon-over-input'>
												<input value="" placeholder="E-mail" class="form-control" data-rule-required="true" name="email" type="text" />
												<i class='icon-user text-muted'></i>
											</div>
										</div>
										<p><button class='btn btn-block'>Восстановить пароль</button></p>
										<p><span class="text-green" id="recovery-send">Ссылка на восстановление пароля отправлена на ваш электронный адрес.</span></p>
										<?=Form::close()?>
										<div class='text-center'>
											<hr class='hr-normal'>
											&larr;&nbsp;<span class="local" id="login-lnk">Авторизация</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- javascript
		================================================== -->
		<? foreach($scripts as $file) { echo HTML::script($file), "\n"; } ?>
    </body>
</html>
