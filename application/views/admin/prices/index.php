<div class='row'>
	<div class='col-sm-12'>
		<div class='box'>
			<div class='box-content box-no-padding list-box'>
				<div class='tabbable'>
					<ul class='nav nav-tabs service-tabs'>
						<li class='active'>
							<a data-toggle='tab' href='#tab1'>
								<i class='icon-star'></i>
								<i class='icon-star'></i>
								<i class='icon-star'></i>
								Бюджетный <small>(от <span class="base-sum" type="1"><?=Num::format($base_prices[1], 0, '',' ')?></span>&mdash;<span class="max-price-budget max-sum" type="1"><?=Num::format($max_prices[1],0,'', ' ')?></span>)</small>
							</a>
						</li>
						<li>
							<a data-toggle='tab' href='#tab2'>
								<i class='icon-star'></i>
								<i class='icon-star'></i>
								<i class='icon-star'></i>
								<i class='icon-star'></i>
								Стандартный <small>(от <span class="base-sum" type="2"><?=Num::format($base_prices[2],0,'',' ')?></span>&mdash;<span class="max-price-standart max-sum" type="2"><?=Num::format($max_prices[2],0,'', ' ')?></span>)</small>
							</a>
						</li>
						<li>
							<a data-toggle='tab' href='#tab3'>
								<i class='icon-star'></i>
								<i class='icon-star'></i>
								<i class='icon-star'></i>
								<i class='icon-star'></i>
								<i class='icon-star'></i>
								Уникальный <small>(от <span class="base-sum" type="3"><?=Num::format($base_prices[3],0,'',' ')?></span>&mdash;<span class="max-price-unique max-sum" type="3"><?=Num::format($max_prices[3],0,'',' ')?></span>)</small>
							</a>
						</li>
					</ul>
					<div class='tab-content'>
						<div class='tab-pane active' id='tab1'>
							<div class="add-service-wrap">
								<?=Form::open('/admin/ajax/add_service', array('class' => 'add-service-form'))?>
									<?=Form::hidden('category', $category)?>
									<?=Form::hidden('type', '1')?>
									<table class="table">
										<tr>
											<td class="thin-col">
												<select name="package">
													<option value="additional" selected="selected">Дополнительная</option>
													<option value="base">Базовая</option>
												</select>
											</td>
											<td><input type="text" name="name" placeholder="Название услуги" class="form-control"></td>
											<td class="thin-col add-service-price"><input type="text" name="price" placeholder="Стоимость" class="form-control price-field"></td>
											<td class="thin-col"><button class="btn btn-success"><i class="icon-plus"></i>&nbsp;&nbsp;Добавить услугу</button></td>
										</tr>
									</table>
								<?=Form::close()?>
							</div>
							<table class="table table-hover">
								<tbody>
									<tr class="warning">
										<th class="sorting"></th>
										<th class="thin-col spacer"></th>
										<th>Базовые услуги</th>
										<th class="thin-col">Стоимость (руб.), итого: <span class="base-sum" type="1"><?=Num::format($base_prices[1],0,'',' ')?></span></th>
										<th class="thin-col"></th>
									</tr>
								</tbody>
							</table>
							<table class="table table-hover prices-table">
								<thead><tr><td></td><td></td><td></td><td></td><td></td></tr></thead>
								<tbody class="prices-list base-services" type="1" package="base">
									<? foreach(ORM::factory('service')->where('category','=',$category)->and_where('type','=','1')->and_where('package','=','base')->find_all() as $service) { ?>
									<tr data-position="<?=$service->sort_order?>" service_id="<?=$service->id?>" id="service_<?=$service->id?>">
										<td class="sorting"><?=$service->sort_order?></td>
										<td class="thin-col"><i class="icon-resize-vertical"></i></td>
										<td><input type="text" name="service[<?=$service->id?>][name]" value='<?=$service->name?>' class="form-control"></td>
										<td class="thin-col"><input type="text" name="service[<?=$service->id?>][price]" value='<?=$service->price?>' class="form-control text-right s-price"></td>
										<td class="thin-col"><button class="btn btn-danger btn-sm delete-service"><i class="icon-remove"></i></button></td>
									</tr>
									<? } ?>
								</tbody>
							</table>
							<table class="table table-hover">
								<tbody>
									<tr class="success">
										<th class="sorting"></th>
										<th class="thin-col spacer"></th>
										<th>Дополнительные услуги</th>
										<th class="thin-col">Стоимость (руб.), итого: <span class="additional-sum" type="1"><?=Num::format($max_prices[1]-$base_prices[1],0,'',' ')?></span></th>
										<th class="thin-col"></th>
									</tr>
								</tbody>
							</table>
							<table class="table table-hover prices-table">
								<thead><tr><td></td><td></td><td></td><td></td><td></td></tr></thead>
								<tbody class="prices-list additional-services" type="1" package="additional">
									<? foreach(ORM::factory('service')->where('category','=',$category)->and_where('type','=','1')->and_where('package','=','additional')->find_all() as $service) { ?>
									<tr data-position="<?=$service->sort_order?>" service_id="<?=$service->id?>" id="service_<?=$service->id?>">
										<td class="sorting"><?=$service->sort_order?></td>
										<td class="thin-col"><i class="icon-resize-vertical"></i></td>
										<td><input type="text" name="service[<?=$service->id?>][name]" value='<?=$service->name?>' class="form-control"></td>
										<td class="thin-col"><input type="text" name="service[<?=$service->id?>][price]" value='<?=$service->price?>' class="form-control text-right s-price"></td>
										<td class="thin-col"><button class="btn btn-danger btn-sm delete-service"><i class="icon-remove"></i></button></td>
									</tr>
									<? } ?>
								</tbody>
							</table>
						</div>
						<div class='tab-pane' id='tab2'>
							<div class="add-service-wrap">
								<?=Form::open('/admin/ajax/add_service', array('class' => 'add-service-form'))?>
									<?=Form::hidden('category', $category)?>
									<?=Form::hidden('type', '2')?>
									<table class="table">
										<tr>
											<td class="thin-col">
												<select name="package">
													<option value="additional" selected="selected">Дополнительная</option>
													<option value="base">Базовая</option>
												</select>
											</td>
											<td><input type="text" name="name" placeholder="Название услуги" class="form-control"></td>
											<td class="thin-col add-service-price"><input type="text" name="price" placeholder="Стоимость" class="form-control price-field"></td>
											<td class="thin-col"><button class="btn btn-success"><i class="icon-plus"></i>&nbsp;&nbsp;Добавить услугу</button></td>
										</tr>
									</table>
								<?=Form::close()?>
							</div>
							<table class="table table-hover">
								<tbody>
									<tr class="warning">
										<th class="sorting"></th>
										<th class="thin-col spacer"></th>
										<th>Базовые услуги</th>
										<th class="thin-col">Стоимость (руб.), итого: <span class="base-sum" type="2"><?=Num::format($base_prices[2],0,'',' ')?></span></th>
										<th class="thin-col"></th>
									</tr>
								</tbody>
							</table>
							<table class="table table-hover prices-table">
								<thead><tr><td></td><td></td><td></td><td></td><td></td></tr></thead>
								<tbody class="prices-list base-services" type="2" package="base">
								<? foreach(ORM::factory('service')->where('category','=',$category)->and_where('type','=','2')->and_where('package','=','base')->find_all() as $service) { ?>
									<tr data-position="<?=$service->sort_order?>" service_id="<?=$service->id?>" id="service_<?=$service->id?>">
										<td class="sorting"><?=$service->sort_order?></td>
										<td class="thin-col"><i class="icon-resize-vertical"></i></td>
										<td><input type="text" name="service[<?=$service->id?>][name]" value='<?=$service->name?>' class="form-control"></td>
										<td class="thin-col"><input type="text" name="service[<?=$service->id?>][price]" value='<?=$service->price?>' class="form-control text-right s-price"></td>
										<td class="thin-col"><button class="btn btn-danger btn-sm delete-service"><i class="icon-remove"></i></button></td>
									</tr>
								<? } ?>
								</tbody>
							</table>
							<table class="table table-hover">
								<tbody>
									<tr class="success">
										<th class="sorting"></th>
										<th class="thin-col spacer"></th>
										<th>Дополнительные услуги</th>
										<th class="thin-col">Стоимость (руб.), итого: <span class="additional-sum" type="2"><?=Num::format($max_prices[2]-$base_prices[2],0,'',' ')?></span></th>
										<th class="thin-col"></th>
									</tr>
								</tbody>
							</table>
							<table class="table table-hover prices-table">
								<thead><tr><td></td><td></td><td></td><td></td><td></td></tr></thead>
								<tbody class="prices-list additional-services" type="2" package="additional">
								<? foreach(ORM::factory('service')->where('category','=',$category)->and_where('type','=','2')->and_where('package','=','additional')->find_all() as $service) { ?>
									<tr data-position="<?=$service->sort_order?>" service_id="<?=$service->id?>" id="service_<?=$service->id?>">
										<td class="sorting"><?=$service->sort_order?></td>
										<td class="thin-col"><i class="icon-resize-vertical"></i></td>
										<td><input type="text" name="service[<?=$service->id?>][name]" value='<?=$service->name?>' class="form-control"></td>
										<td class="thin-col"><input type="text" name="service[<?=$service->id?>][price]" value='<?=$service->price?>' class="form-control text-right s-price"></td>
										<td class="thin-col"><button class="btn btn-danger btn-sm delete-service"><i class="icon-remove"></i></button></td>
									</tr>
								<? } ?>
								</tbody>
							</table>
						</div>
						<div class='tab-pane' id='tab3'>
							<div class="add-service-wrap">
								<?=Form::open('/admin/ajax/add_service', array('class' => 'add-service-form'))?>
									<?=Form::hidden('category', $category)?>
									<?=Form::hidden('type', '3')?>
									<table class="table">
										<tr>
											<td class="thin-col">
												<select name="package">
													<option value="additional" selected="selected">Дополнительная</option>
													<option value="base">Базовая</option>
												</select>
											</td>
											<td><input type="text" name="name" placeholder="Название услуги" class="form-control"></td>
											<td class="thin-col add-service-price"><input type="text" name="price" placeholder="Стоимость" class="form-control price-field"></td>
											<td class="thin-col"><button class="btn btn-success"><i class="icon-plus"></i>&nbsp;&nbsp;Добавить услугу</button></td>
										</tr>
									</table>
								<?=Form::close()?>
							</div>
							<table class="table table-hover">
								<tbody>
									<tr class="warning">
										<th class="sorting"></th>
										<th class="thin-col spacer"></th>
										<th>Базовые услуги</th>
										<th class="thin-col">Стоимость (руб.), итого: <span class="base-sum" type="3"><?=Num::format($base_prices[3],0,'',' ')?></span></th>
										<th class="thin-col"></th>
									</tr>
								</tbody>
							</table>
							<table class="table table-hover prices-table">
								<thead><tr><td></td><td></td><td></td><td></td><td></td></tr></thead>
								<tbody class="prices-list base-services" type="3" package="base">
								<? foreach(ORM::factory('service')->where('category','=',$category)->and_where('type','=','3')->and_where('package','=','base')->find_all() as $service) { ?>
									<tr data-position="<?=$service->sort_order?>" service_id="<?=$service->id?>" id="service_<?=$service->id?>">
										<td class="sorting"><?=$service->sort_order?></td>
										<td class="thin-col"><i class="icon-resize-vertical"></i></td>
										<td><input type="text" name="service[<?=$service->id?>][name]" value='<?=$service->name?>' class="form-control"></td>
										<td class="thin-col"><input type="text" name="service[<?=$service->id?>][price]" value='<?=$service->price?>' class="form-control text-right s-price"></td>
										<td class="thin-col"><button class="btn btn-danger btn-sm delete-service"><i class="icon-remove"></i></button></td>
									</tr>
								<? } ?>
								</tbody>
							</table>
							<table class="table table-hover">
								<tbody>
									<tr class="success">
										<th class="sorting"></th>
										<th class="thin-col spacer"></th>
										<th>Дополнительные услуги</th>
										<th class="thin-col">Стоимость (руб.), итого: <span class="additional-sum" type="3"><?=Num::format($max_prices[3]-$base_prices[3],0,'',' ')?></span></th>
										<th class="thin-col"></th>
									</tr>
								</tbody>
							</table>
							<table class="table table-hover prices-table">
								<thead><tr><td></td><td></td><td></td><td></td><td></td></tr></thead>
								<tbody class="prices-list additional-services" type="3" package="additional">
								<? foreach(ORM::factory('service')->where('category','=',$category)->and_where('type','=','3')->and_where('package','=','additional')->find_all() as $service) { ?>
									<tr data-position="<?=$service->sort_order?>" service_id="<?=$service->id?>" id="service_<?=$service->id?>">
										<td class="sorting"><?=$service->sort_order?></td>
										<td class="thin-col"><i class="icon-resize-vertical"></i></td>
										<td><input type="text" name="service[<?=$service->id?>][name]" value='<?=$service->name?>' class="form-control"></td>
										<td class="thin-col"><input type="text" name="service[<?=$service->id?>][price]" value='<?=$service->price?>' class="form-control text-right s-price"></td>
										<td class="thin-col"><button class="btn btn-danger btn-sm delete-service"><i class="icon-remove"></i></button></td>
									</tr>
								<? } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>