<!DOCTYPE html>
	<head>
		<meta charset="utf-8"/>
		<link rel="icon" type="image/x-icon" href="/assets/img/favicon.ico"/>
		<title><?=$title?></title>
		<?php if($detect->is('iPhone') || $detect->is('iPad')) { ?>
			<meta content="telephone=no" name="format-detection">
		<? } ?>
		<?php if(!$android && !$detect->is('windowsphoneos')) { ?>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
		<? } else if($detect->is('windowsphoneos')) { ?>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1"/>
		<? } ?>
		<!--[if lte IE 8]>
	    <meta http-equiv="refresh" content="0; url=http://browsehappy.com/">
	    <![endif]-->
		<meta name="keywords" content="<?=$meta_keywords;?>" />
		<meta name="description" content="<?=$meta_description;?>" />
		<meta name="copyright" content="<?=$meta_copyright;?>" />
		<meta name="author" content="<?=$meta_author;?>" />
		<? foreach($styles as $file => $type) { echo HTML::style($file, array('media' => $type)), "\n"; } ?>
		<!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
	<body id="home" class="<?php echo $body_class; ?>">
		<?=$content?>

		<!-- Yandex.Metrika counter -->
		<script type="text/javascript">
			(function (d, w, c) {
				(w[c] = w[c] || []).push(function() {
					try {
						w.yaCounter24951449 = new Ya.Metrika({id:24951449,
							webvisor:true,
							clickmap:true,
							trackLinks:true,
							accurateTrackBounce:true});
					} catch(e) { }
				});

				var n = d.getElementsByTagName("script")[0],
					s = d.createElement("script"),
					f = function () { n.parentNode.insertBefore(s, n); };
				s.type = "text/javascript";
				s.async = true;
				s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

				if (w.opera == "[object Opera]") {
					d.addEventListener("DOMContentLoaded", f, false);
				} else { f(); }
			})(document, window, "yandex_metrika_callbacks");
		</script>
		<noscript><div><img src="//mc.yandex.ru/watch/24951449" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
		<!-- /Yandex.Metrika counter -->

		<!-- javascript
		================================================== -->
		<? foreach($scripts as $file) { echo HTML::script($file), "\n"; } ?>
	</body>
</html>