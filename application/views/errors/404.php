<!DOCTYPE html>
	<head>
		<meta charset="utf-8"/>
		<title>Ошибка 404</title>
		<? foreach($styles as $file => $type) { echo HTML::style($file, array('media' => $type)), "\n"; } ?>
	</head>
	<body class="error-page">
		<div class="wrapper price">
			<div class="topNav price" id="nav">
				<a href="/"><img src="/assets/img/logo.svg" alt=""></a>
				<nav>
					<ul>
						<li><a href="/"><span>Главная</span></a></li>
						<li><a href="/prices"><span>Цены</span></a></li>
						<li><a href="/contacts"><span>Контакты</span></a></li>
					</ul>
				</nav>
				<div class="top-phone">
					<h3>+7 (495) 374-64-82</h3>
					<p><a href="mailto:info@siteexpo.ru">info@siteexpo.ru</a></p>
				</div>
			</div>

			<div class="topNavMob">
				<span></span>
			</div>
			<div id="menu">
				<ul>
					<li><a href="/"><span>Главная</span></a></li>
					<li><a href="/prices"><span>Цены</span></a></li>
					<li><a href="/contacts"><span>Контакты</span></a></li>
					<li><a href="tel:+7 (495) 374-64-82">+7 (495) 374-64-82</a></li>
				</ul>
			</div>

			<div class="selection error-body">
				<h1>404</h1>
			</div>
		</div>

		<!-- Yandex.Metrika counter -->
		<script type="text/javascript">
			(function (d, w, c) {
				(w[c] = w[c] || []).push(function() {
					try {
						w.yaCounter24420259 = new Ya.Metrika({id:24420259,
							webvisor:true,
							clickmap:true,
							trackLinks:true,
							accurateTrackBounce:true});
					} catch(e) { }
				});

				var n = d.getElementsByTagName("script")[0],
					s = d.createElement("script"),
					f = function () { n.parentNode.insertBefore(s, n); };
				s.type = "text/javascript";
				s.async = true;
				s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

				if (w.opera == "[object Opera]") {
					d.addEventListener("DOMContentLoaded", f, false);
				} else { f(); }
			})(document, window, "yandex_metrika_callbacks");
		</script>
		<noscript><div><img src="//mc.yandex.ru/watch/24420259" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
		<!-- /Yandex.Metrika counter -->
	</body>
</html>