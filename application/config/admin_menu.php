<?php defined('SYSPATH') or die('No direct access allowed.');
return array(
	'prices' => array(
		'url' => 'prices',
		'icon' => 'money',
		'name' => 'Цены',
		'items' => array(
			'shop' => array(
				'url' => 'shop',
				'icon' => 'shopping-cart',
				'name' => 'Интернет магазин',
			),
			'corporate' => array(
				'url' => 'corporate',
				'icon' => 'briefcase',
				'name' => 'Корпоративный',
			),
			'startup' => array(
				'url' => 'startup',
				'icon' => 'rocket',
				'name' => 'Стартап',
			),
		)
	),
	'requests' => array(
		'url' => 'requests',
		'icon' => 'envelope',
		'name' => 'Заявки',
		'items' => array(
			'new' => array(
				'url' => 'new',
				'icon' => 'bolt',
				'name' => 'Новые',
			),
			'archive' => array(
				'url' => 'archive',
				'icon' => 'archive',
				'name' => 'Архив',
			),
		),
	)
);
?>